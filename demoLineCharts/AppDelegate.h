//
//  AppDelegate.h
//  demoLineCharts
//
//  Created by Thao on 8/25/14.
//  Copyright (c) 2014 thao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (assign, nonatomic) int iPeriod;
@property (strong, nonatomic) NSNumber *iAge;
@property (strong, nonatomic) NSNumber *fHeight;
@property (strong, nonatomic) NSNumber *fWeight;

@end
