//
//  TabBarView.h
//  demoLineCharts
//
//  Created by Thao on 9/13/14.
//  Copyright (c) 2014 thao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabBarView : UITabBarController <UITabBarControllerDelegate>

@end
