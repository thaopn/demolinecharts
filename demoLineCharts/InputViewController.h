//
//  InputViewController.h
//  demoLineCharts
//
//  Created by Thao on 9/13/14.
//  Copyright (c) 2014 thao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InputViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *txtAge;
@property (weak, nonatomic) IBOutlet UITextField *txtHeight;
@property (weak, nonatomic) IBOutlet UITextField *txtWeight;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segPeriod;
- (IBAction)indexChange:(id)sender;


@end
