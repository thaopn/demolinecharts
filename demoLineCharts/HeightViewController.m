//
//  HeightViewController.m
//  demoLineCharts
//
//  Created by Thao on 9/10/14.
//  Copyright (c) 2014 thao. All rights reserved.
//

#import "HeightViewController.h"




#import "WSLineChartView.h"
#import "WSChartObject.h"


@interface HeightViewController (){
    NSArray *arrList;
}

@property (nonatomic, strong) UIScrollView *chartView;

@end

@implementation HeightViewController

@synthesize chartView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *jsonData = @"[{\"thang\": 0		,\"can\":2.9  ,\"cao\": 48.0 	  ,\"canbt\": 3.3   ,\"caobt\": 49.9 	,\"cant\":3.9  ,\"caot\": 51.8 },"
    "{\"thang\": 	1 	,\"can\":3.9  ,\"cao\": 52.8 	  ,\"canbt\": 4.5   ,\"caobt\": 54.9 	,\"cant\":5.1  ,\"caot\": 56.7 },"
    "{\"thang\": 	2 	,\"can\":4.9  ,\"cao\": 56.4 	  ,\"canbt\": 5.6   ,\"caobt\": 58.4 	,\"cant\":6.3  ,\"caot\": 60.4 },"
    "{\"thang\": 	3 	,\"can\":5.6  ,\"cao\": 59.4 	  ,\"canbt\": 6.4   ,\"caobt\": 61.4 	,\"cant\":7.2  ,\"caot\": 63.5 },"
    "{\"thang\": 	4 	,\"can\":6.2  ,\"cao\": 61.8 	  ,\"canbt\": 7.0   ,\"caobt\": 63.9 	,\"cant\":7.9  ,\"caot\": 66.0 },"
    "{\"thang\": 	5 	,\"can\":6.7  ,\"cao\": 63.8 	  ,\"canbt\": 7.5   ,\"caobt\": 65.9 	,\"cant\":8.4  ,\"caot\": 68.0 },"
    "{\"thang\": 	6 	,\"can\":7.1  ,\"cao\": 65.5 	  ,\"canbt\": 7.9   ,\"caobt\": 67.6 	,\"cant\":8.9  ,\"caot\": 69.8 },"
    "{\"thang\": 	7 	,\"can\":7.4  ,\"cao\": 67.0 	  ,\"canbt\": 8.3   ,\"caobt\": 69.2 	,\"cant\":9.3  ,\"caot\": 71.3 },"
    "{\"thang\": 	8 	,\"can\":7.7  ,\"cao\": 68.4 	  ,\"canbt\": 8.6   ,\"caobt\": 70.6 	,\"cant\":9.6  ,\"caot\": 72.8 },"
    "{\"thang\": 	9 	,\"can\":7.9  ,\"cao\": 69.7 	  ,\"canbt\": 8.9   ,\"caobt\": 72.0 	,\"cant\":10.0 ,\"caot\": 74.2 },"
    "{\"thang\": 	10 	,\"can\":8.2  ,\"cao\": 71.0 	  ,\"canbt\": 9.2   ,\"caobt\": 73.3 	,\"cant\":10.3 ,\"caot\": 75.6 },"
    "{\"thang\": 	11 	,\"can\":8.4  ,\"cao\": 72.2      ,\"canbt\": 9.4   ,\"caobt\": 74.5 	,\"cant\":10.5 ,\"caot\": 76.9 },"
    "{\"thang\": 	12 	,\"can\":8.6  ,\"cao\": 73.4 	  ,\"canbt\": 9.6   ,\"caobt\": 75.7 	,\"cant\":10.8 ,\"caot\": 78.1 },"
    "{\"thang\": 	13 	,\"can\":9.2  ,\"cao\": 74.4 	  ,\"canbt\": 10.4  ,\"caobt\": 76.9 	,\"cant\":11.9 ,\"caot\": 79.4 },"
    "{\"thang\": 	14 	,\"can\":9.4  ,\"cao\": 75.5 	  ,\"canbt\": 10.6  ,\"caobt\": 78.0 	,\"cant\":12.1 ,\"caot\": 80.6 },"
    "{\"thang\": 	15 	,\"can\":9.6  ,\"cao\": 76.5 	  ,\"canbt\": 10.9  ,\"caobt\": 79.1 	,\"cant\":12.4 ,\"caot\": 81.8 },"
    "{\"thang\": 	16 	,\"can\":9.8  ,\"cao\": 77.5 	  ,\"canbt\": 11.1  ,\"caobt\": 80.2 	,\"cant\":12.6 ,\"caot\": 82.9 },"
    "{\"thang\": 	17 	,\"can\":9.9  ,\"cao\": 78.5 	  ,\"canbt\": 11.3  ,\"caobt\": 81.2 	,\"cant\":12.8 ,\"caot\": 84.0 },"
    "{\"thang\": 	18 	,\"can\":9.7  ,\"cao\": 79.5 	  ,\"canbt\": 10.9  ,\"caobt\": 82.3 	,\"cant\":12.3 ,\"caot\": 85.1 },"
    "{\"thang\": 	19 	,\"can\":9.2  ,\"cao\": 80.4 	  ,\"canbt\": 10.4  ,\"caobt\": 83.2 	,\"cant\":11.9 ,\"caot\": 86.1 },"
    "{\"thang\": 	20 	,\"can\":9.4  ,\"cao\": 81.3 	  ,\"canbt\": 10.6  ,\"caobt\": 84.2 	,\"cant\":12.1 ,\"caot\": 87.1 },"
    "{\"thang\": 	21 	,\"can\":9.6  ,\"cao\": 82.2 	  ,\"canbt\": 10.9  ,\"caobt\": 85.1 	,\"cant\":12.4 ,\"caot\": 88.1 },"
    "{\"thang\":  22 	,\"can\":9.8  ,\"cao\": 83.0 	  ,\"canbt\": 11.1  ,\"caobt\": 86.0 	,\"cant\":12.6 ,\"caot\": 89.1 },"
    "{\"thang\":  23 	,\"can\":9.9  ,\"cao\": 83.8 	  ,\"canbt\": 11.3  ,\"caobt\": 86.9 	,\"cant\":12.8 ,\"caot\": 90.0 },"
    "{\"thang\":  24 	,\"can\":10.8 ,\"cao\": 83.9 	  ,\"canbt\": 12.2  ,\"caobt\": 87.1 	,\"cant\":13.7 ,\"caot\": 90.3 },"
    "{\"thang\":  25 	,\"can\":10.3 ,\"cao\": 84.7 	  ,\"canbt\": 11.7  ,\"caobt\":88.0 	,\"cant\":13.3 ,\"caot\": 91.2 },"
    "{\"thang\":  26 	,\"can\":10.5 ,\"cao\": 85.5 	  ,\"canbt\": 11.9  ,\"caobt\":88.8 	,\"cant\":13.6 ,\"caot\": 92.1 },"
    "{\"thang\":  27 	,\"can\":10.7 ,\"cao\": 86.3 	  ,\"canbt\": 12.1  ,\"caobt\":89.6 	,\"cant\":13.8 ,\"caot\": 93.0 },"
    "{\"thang\":  28 	,\"can\":10.8 ,\"cao\": 87.0 	  ,\"canbt\": 12.3  ,\"caobt\":90.4 	,\"cant\":14.0 ,\"caot\": 93.8 },"
    "{\"thang\":  29 	,\"can\":11.0 ,\"cao\": 87.7 	  ,\"canbt\": 12.5  ,\"caobt\":91.2 	,\"cant\":14.3 ,\"caot\": 94.7 },"
    "{\"thang\":  30 	,\"can\":11.8 ,\"cao\": 88.4 	  ,\"canbt\": 13.3  ,\"caobt\":91.9 	,\"cant\":15.0 ,\"caot\": 95.5 },"
    "{\"thang\":  31 	,\"can\":11.3 ,\"cao\": 89.1 	  ,\"canbt\": 12.9  ,\"caobt\":92.7 	,\"cant\":14.7 ,\"caot\": 96.2 },"
    "{\"thang\":  32 	,\"can\":11.5 ,\"cao\": 89.7 	  ,\"canbt\": 13.1  ,\"caobt\":93.4 	,\"cant\":15.0 ,\"caot\": 97.0 },"
    "{\"thang\":  33 	,\"can\":11.7 ,\"cao\": 90.4 	  ,\"canbt\": 13.3  ,\"caobt\":94.1 	,\"cant\":15.2 ,\"caot\": 97.8 },"
    "{\"thang\":  34 	,\"can\":11.8 ,\"cao\": 91.0 	  ,\"canbt\": 13.5  ,\"caobt\":94.8 	,\"cant\":15.4 ,\"caot\": 98.5 },"
    "{\"thang\":  35 	,\"can\":12.0 ,\"cao\": 91.6 	  ,\"canbt\": 13.7  ,\"caobt\":95.4 	,\"cant\":15.7 ,\"caot\": 99.2 },"
    "{\"thang\":  36 	,\"can\":12.7 ,\"cao\": 92.2 	  ,\"canbt\": 14.3  ,\"caobt\":96.1 	,\"cant\":16.3 ,\"caot\": 99.9 },"
    "{\"thang\":  37 	,\"can\":12.3 ,\"cao\": 92.8 	  ,\"canbt\": 14.0  ,\"caobt\":96.7 	,\"cant\":16.1 ,\"caot\": 100.6},"
    "{\"thang\":  38 	,\"can\":12.5 ,\"cao\": 93.4 	  ,\"canbt\":14.2   ,\"caobt\":97.4 	,\"cant\":16.3 ,\"caot\": 101.3},"
    "{\"thang\":  39 	,\"can\":12.6 ,\"cao\": 94.0 	  ,\"canbt\":14.4   ,\"caobt\":98.0 	,\"cant\":16.6 ,\"caot\": 102.0},"
    "{\"thang\":  40 	,\"can\":12.8 ,\"cao\": 94.6 	  ,\"canbt\":14.6   ,\"caobt\":98.6 	,\"cant\":16.8 ,\"caot\": 102.7},"
    "{\"thang\":  41 	,\"can\":12.9 ,\"cao\": 95.2 	  ,\"canbt\":14.8   ,\"caobt\":99.2 	,\"cant\":17.0 ,\"caot\": 103.3},"
    "{\"thang\":  42 	,\"can\":13.5 ,\"cao\": 95.7 	  ,\"canbt\":15.3   ,\"caobt\": 99.9 	,\"cant\":17.5 ,\"caot\": 104.0},"
    "{\"thang\":  43 	,\"can\":13.2 ,\"cao\": 96.3 	  ,\"canbt\":15.2   ,\"caobt\":100.4 	,\"cant\":17.5 ,\"caot\": 104.6},"
    "{\"thang\":  44 	,\"can\":13.4 ,\"cao\": 96.8 	  ,\"canbt\":15.3   ,\"caobt\":101.0 	,\"cant\":17.7 ,\"caot\": 105.2},"
    "{\"thang\":  45 	,\"can\":13.5 ,\"cao\": 97.4 	  ,\"canbt\":15.5   ,\"caobt\":101.6 	,\"cant\":17.9 ,\"caot\": 105.8},"
    "{\"thang\":  46 	,\"can\":13.7 ,\"cao\": 97.9 	  ,\"canbt\":15.7   ,\"caobt\":102.2 	,\"cant\":18.2 ,\"caot\": 106.5},"
    "{\"thang\":  47 	,\"can\":13.8 ,\"cao\": 98.5 	  ,\"canbt\":15.9   ,\"caobt\":102.8 	,\"cant\":18.4 ,\"caot\": 107.1},"
    "{\"thang\":  48 	,\"can\":14.3,\"cao\":  99.0 	  ,\"canbt\":16.3   ,\"caobt\":103.3 	,\"cant\":18.7 ,\"caot\": 107.7},"
    "{\"thang\":  49 	,\"can\":14.1,\"cao\":  99.5 	  ,\"canbt\":16.3   ,\"caobt\":103.9 	,\"cant\":18.9 ,\"caot\": 108.3},"
    "{\"thang\":  50 	,\"can\":14.3,\"cao\":  100.0 	  ,\"canbt\":16.4   ,\"caobt\":104.4 	,\"cant\":19.1 ,\"caot\": 108.9},"
    "{\"thang\":  51 	,\"can\":14.4,\"cao\":  100.5 	  ,\"canbt\":16.6   ,\"caobt\":105.0 	,\"cant\":19.3 ,\"caot\": 109.5},"
    "{\"thang\":  52 	,\"can\":14.5,\"cao\":  101.1 	  ,\"canbt\":16.8   ,\"caobt\":105.6 	,\"cant\":19.5 ,\"caot\": 110.1},"
    "{\"thang\":  53 	,\"can\":14.7,\"cao\":  101.6 	  ,\"canbt\":17.0   ,\"caobt\":106.1 	,\"cant\":19.8 ,\"caot\": 110.7},"
    "{\"thang\":  54 	,\"can\":15.2,\"cao\":  102.1 	  ,\"canbt\":17.3   ,\"caobt\":106.7 	,\"cant\":19.9 ,\"caot\": 111.2},"
    "{\"thang\":  55 	,\"can\":15.0,\"cao\":  102.6 	  ,\"canbt\":17.3   ,\"caobt\":107.2 	,\"cant\":20.2 ,\"caot\": 111.8},"
    "{\"thang\":  56 	,\"can\":15.1,\"cao\":  103.1 	  ,\"canbt\":17.5   ,\"caobt\":107.8 	,\"cant\":20.4 ,\"caot\": 112.4},"
    "{\"thang\":  57 	,\"can\":15.3,\"cao\":  103.6 	  ,\"canbt\":17.7   ,\"caobt\":108.3 	,\"cant\":20.7 ,\"caot\": 113.0},"
    "{\"thang\":  58 	,\"can\":15.4,\"cao\":  104.1 	  ,\"canbt\":17.9   ,\"caobt\":108.9 	,\"cant\":20.9 ,\"caot\": 113.6},"
    "{\"thang\":  59 	,\"can\":15.5,\"cao\":  104.7 	  ,\"canbt\":18.0   ,\"caobt\":109.4 	,\"cant\":21.1 ,\"caot\": 114.2},"
    "{\"thang\":  60 	,\"can\":16.0,\"cao\":  105.2 	  ,\"canbt\":18.3   ,\"caobt\":110.0 	,\"cant\":21.1 ,\"caot\": 114.8} ]";
    
    arrList = [NSJSONSerialization JSONObjectWithData:[jsonData dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    //NSLog(@"%@", arrList);
    
    
	// Do any additional setup after loading the view, typically from a nib.
    CGRect c = self.view.frame;
    //c.size.height-=80;
    
    self.chartView = [[UIScrollView alloc] initWithFrame:c];
    self.chartView.contentSize = CGSizeMake(1400, 400); // Have to set a size here related to your content.
    // You should set these.
    self.chartView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    
    
    self.chartView.minimumZoomScale = .5;  // You will have to set some values for these
    self.chartView.maximumZoomScale = 2.0;
    self.chartView.zoomScale = 1;
    self.chartView.userInteractionEnabled = true;
    [self.view insertSubview:self.chartView atIndex:0];
    [self createLineChart];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)createLineChart
{
    WSLineChartView *lineChart = [[WSLineChartView alloc] initWithFrame:CGRectMake(0, 0, 1400, 400)];//CGRectMake(10.0, 50.0, 800.0, 400.0)];
    
    NSMutableArray *arr = [self createDemoDatas:30];
    NSDictionary *colorDict = [self createColorDict];
    
    lineChart.xAxisName = @"Tháng tuổi";
    lineChart.rowWidth = 20.0;
    lineChart.title = @"Biểu đồ chiều cao";
    lineChart.showZeroValueOnYAxis = NO;
    //lineChart.titleFrame = CGRectMake(0.0, 0.0, 400, 50);
    //lineChart.legendFrame = CGRectMake(0.0, 0.0, 400, 400);
    [lineChart drawChart:arr withColor:colorDict];
    lineChart.backgroundColor = [UIColor whiteColor];
    [self.chartView addSubview:lineChart];
}

- (NSMutableArray*)createDemoDatas:(int)count
{
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    for (int i=0; i<arrList.count; i++) {
        WSChartObject *suyObj = [[WSChartObject alloc] init];
        suyObj.name = @"Suy Dinh Dưỡng";
        suyObj.xValue = [NSString stringWithFormat:@"%d",i];
        suyObj.yValue = [NSNumber numberWithFloat: [arrList[i][@"cao"] floatValue]];
        WSChartObject *binhObj = [[WSChartObject alloc] init];
        binhObj.name = @"Bình Thường";
        binhObj.xValue = [NSString stringWithFormat:@"%d",i];
        binhObj.yValue = [NSNumber numberWithFloat:[arrList[i][@"caobt"] floatValue]];
        //        WSChartObject *youObj = [[WSChartObject alloc] init];
        //        youObj.name = @"Con Bạn";
        //        youObj.xValue = [NSString stringWithFormat:@"%d",i];
        //        youObj.yValue = [NSNumber numberWithFloat:[arrList[i][@"can"] floatValue]];
        WSChartObject *thuaObj = [[WSChartObject alloc] init];
        thuaObj.name = @"Thừa Cân";
        thuaObj.xValue = [NSString stringWithFormat:@"%d",i];
        thuaObj.yValue = [NSNumber numberWithFloat:[arrList[i][@"caot"] floatValue]];
        
        //        WSChartObject *avgObj = [[WSChartObject alloc] init];
        //        avgObj.name = @"Average";
        //        avgObj.xValue = [NSString stringWithFormat:@"%d",i];
        //        avgObj.yValue = [NSNumber numberWithFloat:([lfcObj.yValue floatValue] + [chObj.yValue floatValue] + [muObj.yValue floatValue] + [muObj.yValue floatValue])/4.0];
        
        //        NSDictionary *data = [[NSDictionary alloc] initWithObjectsAndKeys:lfcObj,@"Liverpool",
        //                              muObj,@"MU",
        //                              chObj,@"Chelsea",
        //                              mcObj,@"ManCity",
        //                              avgObj,@"Average",nil];
        NSDictionary *data = [[NSDictionary alloc] initWithObjectsAndKeys:suyObj,@"Suy dinh dưỡng",
                              binhObj,@"Bình thường",
                              thuaObj,@"Thừa cân",nil];
        [arr addObject:data];
    }
    return arr;
}

- (NSDictionary*)createColorDict
{
    NSDictionary *colorDict = [[NSDictionary alloc] initWithObjectsAndKeys:[UIColor redColor],@"Suy dinh dưỡng",
                               [UIColor greenColor],@"Con bạn",
                               [UIColor orangeColor],@"Thừa cân",
                               [UIColor blueColor],@"Bình thường", nil];
    return colorDict;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

@end