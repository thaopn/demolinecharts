//
//  InputViewController.m
//  demoLineCharts
//
//  Created by Thao on 9/13/14.
//  Copyright (c) 2014 thao. All rights reserved.
//

#import "InputViewController.h"
#import "TabBarView.h"
#import "ViewController.h"
#import "AppDelegate.h"

@interface InputViewController ()

@end

@implementation InputViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    UITapGestureRecognizer *tapOnView = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapedOnView:)];
    [self.view addGestureRecognizer:tapOnView];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"FromInputView"]) {
        AppDelegate *delegate = [UIApplication sharedApplication].delegate;
        //delegate.iPeriod = _segPeriod.selectedSegmentIndex;
        //delegate.fAge =
        delegate.iPeriod = 0;
        delegate.iAge = [NSNumber numberWithInt:8];
        delegate.fHeight = [NSNumber numberWithFloat:80.0];
        delegate.fWeight = [NSNumber numberWithFloat:7.0];
    }
}
/*
-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    if ([_txtAge.text intValue]>60 || [_txtAge.text isEqualToString:@""]) {
        [self showAlertView:@"Tuổi không hợp lệ"];
        return NO;
    }else{
        switch (_segPeriod.selectedSegmentIndex) {
            case 0:
                if ([_txtAge.text intValue]<0 || [_txtAge.text intValue]>12) {
                    [self showAlertView:@"Tuổi không hợp lệ"];
                    return NO;
                }
                break;
            case 1:
                if ([_txtAge.text intValue]<13 || [_txtAge.text intValue]>24) {
                    [self showAlertView:@"Tuổi không hợp lệ"];
                    return NO;
                }
                break;
            case 2:
                if ([_txtAge.text intValue]<25 || [_txtAge.text intValue]>36) {
                    [self showAlertView:@"Tuổi không hợp lệ"];
                    return NO;
                }
                break;
            case 3:
                if ([_txtAge.text intValue]<37 || [_txtAge.text intValue]>48) {
                    [self showAlertView:@"Tuổi không hợp lệ"];
                    return NO;
                }
                break;
            case 4:
                if ([_txtAge.text intValue]<49 || [_txtAge.text intValue]>60) {
                    [self showAlertView:@"Tuổi không hợp lệ"];
                    return NO;
                }
                break;
         }
    }
//    if ([_txtHeight.text isEqualToString:@""]) {
//        [self showAlertView:@"Hãy nhập chiều cao"];
//        return NO;
//    }
//    if ([_txtWeight.text isEqualToString:@""]) {
//        [self showAlertView:@"Hãy nhập cân nặng"];
//        return NO;
//    }
    return YES;
}
*/
- (IBAction)indexChange:(id)sender {
    switch (_segPeriod.selectedSegmentIndex) {
        case 0:
            _txtAge.placeholder = @"Tuổi (0-12 tháng)";
            break;
        case 1:
            _txtAge.placeholder = @"Tuổi (13-24 tháng)";
            break;
        case 2:
            _txtAge.placeholder = @"Tuổi (25-36 tháng)";
            break;
        case 3:
            _txtAge.placeholder = @"Tuổi (37-48 tháng)";
            break;
        case 4:
            _txtAge.placeholder = @"Tuổi (49-60 tháng)";
            break;
        default:
            break;
    }
}

-(void)showAlertView:(NSString*)content{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:content delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}
-(void)tapedOnView:(UITapGestureRecognizer*)tapOnView{
    [self.view endEditing:YES];
}
@end
