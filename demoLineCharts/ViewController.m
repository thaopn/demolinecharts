//
//  ViewController.m
//  demoLineCharts
//
//  Created by Thao on 8/25/14.
//  Copyright (c) 2014 thao. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"

#import "WSLineChartView.h"
#import "WSChartObject.h"


@interface ViewController (){
    NSArray *arrList;
}

@property (nonatomic, strong) UIScrollView *chartView;

@end

@implementation ViewController

@synthesize chartView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *jsonData = @"[{\"thang\":0	   ,\"can\": 2.8  ,\"cao\": 47.3  	,\"canbt\":3.2  ,\"caobt\": 49.1  	,\"cant\":3.7  ,\"caot\": 51.0 },"
        "{\"thang\":1 	   ,\"can\": 3.6  ,\"cao\": 51.7  	,\"canbt\":4.2  ,\"caobt\": 53.7  	,\"cant\":4.8  ,\"caot\": 55.6 },"
        "{\"thang\":2 	   ,\"can\": 4.5  ,\"cao\": 55.0  	,\"canbt\":5.1  ,\"caobt\": 57.1  	,\"cant\":5.9  ,\"caot\": 59.1 },"
        "{\"thang\":3 	   ,\"can\": 5.1  ,\"cao\": 57.7  	,\"canbt\":5.8  ,\"caobt\": 59.8  	,\"cant\":6.7  ,\"caot\": 61.9 },"
        "{\"thang\":4 	   ,\"can\": 5.6  ,\"cao\": 59.9  	,\"canbt\":6.4  ,\"caobt\": 62.1  	,\"cant\":7.3  ,\"caot\": 64.3 },"
        "{\"thang\":5 	   ,\"can\": 6.1  ,\"cao\": 61.8  	,\"canbt\":6.9  ,\"caobt\": 64.0  	,\"cant\":7.9  ,\"caot\": 66.2 },"
        "{\"thang\":6 	   ,\"can\": 6.4  ,\"cao\": 63.5  	,\"canbt\":7.3  ,\"caobt\": 65.7  	,\"cant\":8.3  ,\"caot\": 68.0 },"
        "{\"thang\":7 	   ,\"can\": 6.7  ,\"cao\": 65.0  	,\"canbt\":7.6  ,\"caobt\": 67.3  	,\"cant\":8.7  ,\"caot\": 69.6 },"
        "{\"thang\":8 	   ,\"can\": 7.0  ,\"cao\": 66.4  	,\"canbt\":7.9  ,\"caobt\": 68.7  	,\"cant\":9.0  ,\"caot\": 71.1 },"
        "{\"thang\":9 	   ,\"can\": 7.3  ,\"cao\": 67.7  	,\"canbt\":8.2  ,\"caobt\": 70.1  	,\"cant\":9.3  ,\"caot\": 72.6 },"
        "{\"thang\":10 	,\"can\":7.5  ,\"cao\": 69.0  	,\"canbt\":8.5  ,\"caobt\": 71.5  	,\"cant\":9.6  ,\"caot\": 73.9 },"
        "{\"thang\":11 	,\"can\":7.7  ,\"cao\": 70.3  	,\"canbt\":8.7  ,\"caobt\": 72.8  	,\"cant\":9.9  ,\"caot\": 75.3 },"
        "{\"thang\":12 	,\"can\":7.9  ,\"cao\": 71.4  	,\"canbt\":8.9  ,\"caobt\": 74.0  	,\"cant\":10.2 ,\"caot\": 76.6 },"
        "{\"thang\":13 	,\"can\":8.1  ,\"cao\": 72.5  	,\"canbt\":9.2  ,\"caobt\": 75.2  	,\"cant\":10.4 ,\"caot\": 77.9 },"
        "{\"thang\":14 	,\"can\":8.3  ,\"cao\": 73.6  	,\"canbt\":9.4  ,\"caobt\": 76.4  	,\"cant\":10.7 ,\"caot\": 79.2 },"
        "{\"thang\":15 	,\"can\":8.5  ,\"cao\": 74.7  	,\"canbt\":9.6  ,\"caobt\": 77.5  	,\"cant\":10.9 ,\"caot\": 80.3 },"
        "{\"thang\":16 	,\"can\":8.7  ,\"cao\": 75.7  	,\"canbt\":9.8  ,\"caobt\": 78.6  	,\"cant\":11.2 ,\"caot\": 81.5 },"
        "{\"thang\":17 	,\"can\":8.8  ,\"cao\": 76.7  	,\"canbt\":10.0 ,\"caobt\": 79.7  	,\"cant\":11.4 ,\"caot\": 82.6 },"
        "{\"thang\":18 	,\"can\":9.0  ,\"cao\": 77.8  	,\"canbt\":10.2 ,\"caobt\": 80.7  	,\"cant\":11.6 ,\"caot\": 83.6 },"
        "{\"thang\":19 	,\"can\":9.2  ,\"cao\": 78.7  	,\"canbt\":10.4 ,\"caobt\": 81.7  	,\"cant\":11.9 ,\"caot\": 84.8 },"
        "{\"thang\":20 	,\"can\":9.4  ,\"cao\": 79.6  	,\"canbt\":10.6 ,\"caobt\": 82.7  	,\"cant\":12.1 ,\"caot\": 85.8 },"
        "{\"thang\":21 	,\"can\":9.6  ,\"cao\": 80.5  	,\"canbt\":10.9 ,\"caobt\": 83.7  	,\"cant\":12.4 ,\"caot\": 86.8 },"
        "{\"thang\":22 	,\"can\":9.8  ,\"cao\": 81.4  	,\"canbt\":11.1 ,\"caobt\": 84.6  	,\"cant\":12.6 ,\"caot\": 87.8 },"
        "{\"thang\":23 	,\"can\":9.9  ,\"cao\": 82.2  	,\"canbt\":11.3 , \"caobt\": 85.5  	,\"cant\":12.8 ,\"caot\": 88.8 },"
        "{\"thang\":24 	,\"can\":10.1  ,\"cao\": 83.2  	,\"canbt\":11.5 , \"caobt\":  86.4  	,\"cant\":13.1  ,\"caot\":89.6 },"
        "{\"thang\":25 	,\"can\":10.3  ,\"cao\":83.2  	,\"canbt\":11.7 , \"caobt\": 86.6  	    ,\"cant\":13.3  ,\"caot\":90.0 },"
        "{\"thang\":26 	,\"can\":10.5  ,\"cao\":84.0  	,\"canbt\":11.9 , \"caobt\": 87.4  	    ,\"cant\":13.6  ,\"caot\":90.9 },"
        "{\"thang\":27 	,\"can\":10.7  ,\"cao\":84.8  	,\"canbt\":12.1 , \"caobt\": 88.3      	,\"cant\":13.8  ,\"caot\":91.8 },"
        "{\"thang\":28 	,\"can\":10.8  ,\"cao\":85.5  	,\"canbt\":12.3 , \"caobt\": 89.1  	    ,\"cant\":14.0  ,\"caot\":92.7 },"
        "{\"thang\":29 	,\"can\":11.0  ,\"cao\":86.3  	,\"canbt\":12.5 , \"caobt\": 89.9  	    ,\"cant\":14.3  ,\"caot\":93.5 },"
        "{\"thang\":30 	,\"can\":11.2  ,\"cao\": 87.1  	,\"canbt\":12.7 , \"caobt\":  90.7  	,\"cant\":14.5  ,\"caot\":94.2 },"
        "{\"thang\":31 	,\"can\":11.3  ,\"cao\":87.7  	,\"canbt\":12.9 , \"caobt\": 91.4  	    ,\"cant\":14.7  ,\"caot\":95.2 },"
        "{\"thang\":32 	,\"can\":11.5  ,\"cao\":88.4  	,\"canbt\":13.1 , \"caobt\": 92.2  	    ,\"cant\":15.0  ,\"caot\":95.9 },"
        "{\"thang\":33 	,\"can\":11.7  ,\"cao\":89.1  	,\"canbt\":13.3 , \"caobt\": 92.9  	    ,\"cant\":15.2  ,\"caot\":96.7 },"
        "{\"thang\":34 	,\"can\":11.8  ,\"cao\":89.8  	,\"canbt\":13.5 , \"caobt\": 93.6  	    ,\"cant\":15.4  ,\"caot\":97.5 },"
        "{\"thang\":35 	,\"can\":12.0  ,\"cao\":90.5  	,\"canbt\":13.7 , \"caobt\": 94.4  	    ,\"cant\":15.7  ,\"caot\":98.3 },"
        "{\"thang\":36 	,\"can\":12.1  ,\"cao\": 91.2  	,\"canbt\":13.9 , \"caobt\":  95.1  	,\"cant\":15.9  ,\"caot\":98.9 },"
        "{\"thang\":37 	,\"can\":12.3  ,\"cao\":91.7  	,\"canbt\":14.0 , \"caobt\": 95.7  	    ,\"cant\":16.1  ,\"caot\":99.7 },"
        "{\"thang\":38 	,\"can\":12.5  ,\"cao\":92.4  	,\"canbt\":14.2 , \"caobt\": 96.4  	    ,\"cant\":16.3  ,\"caot\":100.5 },"
        "{\"thang\":39 	,\"can\":12.6  ,\"cao\":93.0  	,\"canbt\":14.4 , \"caobt\": 97.1  	    ,\"cant\":16.6  ,\"caot\":101.2 },"
        "{\"thang\":40 	,\"can\":12.8  ,\"cao\":93.6  	,\"canbt\":14.6 , \"caobt\": 97.7  	    ,\"cant\":16.8  ,\"caot\":101.9 },"
        "{\"thang\":41 	,\"can\":12.9  ,\"cao\":94.2  	,\"canbt\":14.8 , \"caobt\": 98.4  	    ,\"cant\":17.0  ,\"caot\":102.6 },"
        "{\"thang\":42 	,\"can\":13.1  ,\"cao\": 95.0  	,\"canbt\":15.0 , \"caobt\": 99.0  	    ,\"cant\":17.3  ,\"caot\":103.1 },"
        "{\"thang\":43 	,\"can\":13.2  ,\"cao\":95.4  	,\"canbt\":15.2 , \"caobt\": 99.7  	    ,\"cant\":17.5  ,\"caot\":103.9 },"
        "{\"thang\":44 	,\"can\":13.4  ,\"cao\":96.0  	,\"canbt\":15.3 , \"caobt\": 100.3  	,\"cant\":17.7  ,\"caot\":104.6 },"
        "{\"thang\":45 	,\"can\":13.5  ,\"cao\":96.6  	,\"canbt\":15.5 , \"caobt\": 100.9  	,\"cant\":17.9  ,\"caot\":105.3 },"
        "{\"thang\":46 	,\"can\":13.7  ,\"cao\":97.2  	,\"canbt\":15.7 , \"caobt\": 101.5  	,\"cant\":18.2  ,\"caot\":105.9 },"
        "{\"thang\":47 	,\"can\":13.8  ,\"cao\":97.7  	,\"canbt\":15.9 , \"caobt\": 102.1  	,\"cant\":18.4  ,\"caot\":106.6 },"
        "{\"thang\":48 	,\"can\":14.0  ,\"cao\": 98.4  	,\"canbt\":16.1 , \"caobt\":  102.7  	,\"cant\":18.6  ,\"caot\": 107.0 },"
        "{\"thang\":49 	,\"can\":14.1  ,\"cao\":98.8  	,\"canbt\":16.3 , \"caobt\": 103.3  	,\"cant\":18.9  ,\"caot\":107.8 },"
        "{\"thang\":50 	,\"can\":14.3  ,\"cao\":99.4  	,\"canbt\":16.4 , \"caobt\": 103.9  	,\"cant\":19.1  ,\"caot\":108.4 },"
        "{\"thang\":51 	,\"can\":14.4  ,\"cao\":99.9  	,\"canbt\":16.6 , \"caobt\": 104.5  	,\"cant\":19.3  ,\"caot\":109.1 },"
        "{\"thang\":52 	,\"can\":14.5  ,\"cao\":100.4  	,\"canbt\":16.8 , \"caobt\": 105.0  	,\"cant\":19.5  ,\"caot\":109.7 },"
        "{\"thang\":53 	,\"can\":14.7  ,\"cao\":101.0  	,\"canbt\":17.0 , \"caobt\": 105.6  	,\"cant\":19.8  ,\"caot\":110.3 },"
        "{\"thang\":54 	,\"can\":14.8  ,\"cao\": 101.6  ,\"canbt\":17.2 , \"caobt\":  106.2  	,\"cant\":20.0  ,\"caot\": 110.7},"
        "{\"thang\":55 	,\"can\":15.0  ,\"cao\":102.0  	,\"canbt\":17.3 , \"caobt\": 106.7  	,\"cant\":20.2  ,\"caot\":111.5 },"
        "{\"thang\":56 	,\"can\":15.1  ,\"cao\":102.5  	,\"canbt\":17.5 , \"caobt\": 107.3  	,\"cant\":20.4  ,\"caot\":112.1 },"
        "{\"thang\":57 	,\"can\":15.3  ,\"cao\":103.0  	,\"canbt\":17.7 , \"caobt\": 107.8  	,\"cant\":20.7  ,\"caot\":112.6 },"
        "{\"thang\":58 	,\"can\":15.4  ,\"cao\":103.5  	,\"canbt\":17.9 , \"caobt\": 108.4  	,\"cant\":20.9  ,\"caot\":113.2 },"
        "{\"thang\":59 	,\"can\":15.5  ,\"cao\":104.0  	,\"canbt\":18.0 , \"caobt\": 108.9  	,\"cant\":21.1  ,\"caot\":113.8 },"
        "{\"thang\":60 	,\"can\":15.7  ,\"cao\": 104.7  ,\"canbt\":18.2 , \"caobt\":  109.4  	,\"cant\":21.3  ,\"caot\":114.2 }]";
    
    arrList = [NSJSONSerialization JSONObjectWithData:[jsonData dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    //NSLog(@"%@", arrList);

    
	// Do any additional setup after loading the view, typically from a nib.
    self.chartView = [[UIScrollView alloc] initWithFrame:self.view.frame];
    self.chartView.contentSize = CGSizeMake(1900, 400); // Have to set a size here related to your content.
    // You should set these.
    self.chartView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    
    
    self.chartView.minimumZoomScale = .5;  // You will have to set some values for these
    self.chartView.maximumZoomScale = 2.0;
    self.chartView.zoomScale = 1;
    self.chartView.userInteractionEnabled = true;
    [self.view insertSubview:self.chartView atIndex:0];
    
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    [self createLineChart:delegate.iPeriod];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)createLineChart:(int)period
{
    WSLineChartView *lineChart = [[WSLineChartView alloc] initWithFrame:CGRectMake(0, 0, 1900, 400)];//CGRectMake(10.0, 50.0, 800.0, 400.0)];
    int min, max;
    switch (period) {
        case 0:
            min = 0;
            max = 12;
            break;
        case 1:
            min = 13;
            max = 24;
            break;
        case 2:
            min = 25;
            max = 36;
            break;
        case 3:
            min = 37;
            max = 48;
            break;
        case 4:
            min = 49;
            max = 60;
            break;
            
        default:
            break;
    }
    NSMutableArray *arr = [self createDemoDatas:min andmax:max];
    NSDictionary *colorDict = [self createColorDict];
    
    lineChart.xAxisName = @"Tháng tuổi";
    lineChart.rowWidth = 30.0;
    lineChart.title = @"Biểu đồ cân nặng";
    lineChart.showZeroValueOnYAxis = NO;
    //lineChart.titleFrame = CGRectMake(0.0, 0.0, 400, 50);
    //lineChart.legendFrame = CGRectMake(0.0, 0.0, 400, 400);
    [lineChart drawChart:arr withColor:colorDict];
    lineChart.backgroundColor = [UIColor whiteColor];
    [self.chartView addSubview:lineChart];
}

- (NSMutableArray*)createDemoDatas:(int)min andmax:(int)max
{
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    for (int i=min; i<=max; i++) {
        //NSLog(@"%@",arrList[i][@"thang"]);
        WSChartObject *suyObj = [[WSChartObject alloc] init];
        suyObj.name = @"Suy Dinh Dưỡng";
        suyObj.xValue = [NSString stringWithFormat:@"%d",i];
        suyObj.yValue = [NSNumber numberWithFloat: [arrList[i][@"can"] floatValue]];
        WSChartObject *binhObj = [[WSChartObject alloc] init];
        binhObj.name = @"Bình Thường";
        binhObj.xValue = [NSString stringWithFormat:@"%d",i];
        binhObj.yValue = [NSNumber numberWithFloat:[arrList[i][@"canbt"] floatValue]];
        
        WSChartObject *youObj = [[WSChartObject alloc] init];
        youObj.name = @"Con Bạn";
        youObj.xValue = [NSString stringWithFormat:@"%d",[((AppDelegate*)[UIApplication sharedApplication].delegate).iAge intValue]];
        youObj.yValue = [NSNumber numberWithFloat:[((AppDelegate*)[UIApplication sharedApplication].delegate).fWeight floatValue]];
        //NSLog(@"%@ - %d - %f",youObj.name,[youObj.xValue intValue],[youObj.yValue floatValue]);
        WSChartObject *thuaObj = [[WSChartObject alloc] init];
        thuaObj.name = @"Thừa Cân";
        thuaObj.xValue = [NSString stringWithFormat:@"%d",i];
        thuaObj.yValue = [NSNumber numberWithFloat:[arrList[i][@"cant"] floatValue]];
        
//        WSChartObject *avgObj = [[WSChartObject alloc] init];
//        avgObj.name = @"Average";
//        avgObj.xValue = [NSString stringWithFormat:@"%d",i];
//        avgObj.yValue = [NSNumber numberWithFloat:([lfcObj.yValue floatValue] + [chObj.yValue floatValue] + [muObj.yValue floatValue] + [muObj.yValue floatValue])/4.0];
        
//        NSDictionary *data = [[NSDictionary alloc] initWithObjectsAndKeys:lfcObj,@"Liverpool",
//                              muObj,@"MU",
//                              chObj,@"Chelsea",
//                              mcObj,@"ManCity",
//                              avgObj,@"Average",nil];
        NSDictionary *data = [[NSDictionary alloc] initWithObjectsAndKeys:suyObj,@"Suy dinh dưỡng",
                              binhObj,@"Bình thường",
                              thuaObj,@"Thừa cân",
                              youObj,@"Con bạn",nil];
        [arr addObject:data];
    }
    return arr;
}

- (NSDictionary*)createColorDict
{
    NSDictionary *colorDict = [[NSDictionary alloc] initWithObjectsAndKeys:[UIColor redColor],@"Suy dinh dưỡng",
                               [UIColor greenColor],@"Con bạn",
                               [UIColor orangeColor],@"Thừa cân",
                               [UIColor blueColor],@"Bình thường", nil];
    return colorDict;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

@end
